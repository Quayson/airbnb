import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userIsAuthenticated = true;
  private _userID = 'edward';

  constructor() { }

  get userIsAuthenticated() {
    return this._userIsAuthenticated;
  }

  get userID() {
    return this._userID;
  }

  login() {
    this._userIsAuthenticated = true;
  }

  logout() {
    this._userIsAuthenticated = false;
  }
}
