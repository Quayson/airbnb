import { PlacesPage } from './places.page';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        // Path for tabs
        path: 'tabs',
        component: PlacesPage,
        children: [
            // paths for dicover and sub paths inside discover page
            {
                path: 'discover',
                children: [
                    {
                        path: '',
                        loadChildren: './discover/discover.module#DiscoverPageModule'
                    },
                    {
                        path: ':placeID',
                        loadChildren: './discover/place-detail/place-detail.module#PlaceDetailPageModule'
                    },

                ]
            },
            {
                path: 'offers',
                children: [
                    {
                        // offers page path
                        path: '',
                        loadChildren: './offers/offers.module#OffersPageModule'
                    },
                    {
                        // new offer page path
                        path: 'new',
                        loadChildren: './offers/new-offer/new-offer.module#NewOfferPageModule'
                    },
                    {
                        // edit offer page path
                        path: 'edit/:placeID',
                        loadChildren: './offers/edit-offer//edit-offer.module#EditOfferPageModule'
                    },
                ]
            },
            {
                path: '',
                redirectTo: '/places/tabs/discover',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/places/tabs/discover',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlacesRoutingModule {

}
