import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';


import { PlacesService } from './../../places.service';
import { Place } from './../../place.model';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit, OnDestroy {

  place: Place;
  isLoading = false;
  placeID: string;
  private placeSub: Subscription;

  editOfferForm: FormGroup;

  constructor(private route: ActivatedRoute, private placesService: PlacesService,
    private navCtrl: NavController, private loadingCtrl: LoadingController,
    private router: Router, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('placeID')) {
        this.navCtrl.navigateBack('/places/tabs/offers');
        return;
      }
      this.isLoading = true;
      this.placeID = paramMap.get('placeID');
      this.placeSub = this.placesService.getPlace(paramMap.get('placeID')).subscribe(place => {
        this.place = place;
        this.editOfferForm = new FormGroup({
          title: new FormControl(this.place.title, {
            updateOn: 'blur',
            validators: [Validators.required]
          }),
          description: new FormControl(this.place.description, {
            updateOn: 'blur',
            validators: [Validators.required, Validators.maxLength(240)]
          })
        });
        this.isLoading = false;
      }, error => {
        this.alertCtrl.create({
          header: 'Error Detected.',
          message: 'Place could not be fetched, please try again later.',
          buttons: [
            {
              text: 'Okay',
              handler: () => {
                this.router.navigate(['/places/tabs/offers']);
              }
            }
          ]
        }).then(alertEl => {
          alertEl.present();
        });
      });
    });
  }

  ngOnDestroy() {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }

  onEditOffer() {
    if (!this.editOfferForm.valid) {
      return;
    }

    this.loadingCtrl.create({
      message: 'Updating place...'
    }).then(loadingEl => {
      loadingEl.present();
      this.placesService.updatePlace(
        this.place.id,
        this.editOfferForm.value.title,
        this.editOfferForm.value.description,
      ).subscribe(() => {
        loadingEl.dismiss();
        this.editOfferForm.reset();
        this.router.navigate(['/places/tabs/offers']);
      }, error => {
        this.alertCtrl.create({
          header: 'Updating Error',
          message: 'Error updating place, please try again later.',
          buttons: [
            {
              text: 'Okay',
              handler: () => {
                this.router.navigate(['/places/tabs/offers']);
              }
            }
          ]
        }).then(alertEl => {
          alertEl.present();
        });
      });
    });
  }
}
