import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { IonItemSliding, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { BookingService } from './booking.service';
import { Booking } from './booking.model';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {

  loadedBookings: Booking[];
  isLoading = false;
  private bookingSub: Subscription;

  constructor(private bookingService: BookingService, private router: Router,
    private loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.bookingSub = this.bookingService.bookings.subscribe(bookings => {
      this.loadedBookings = bookings;
    });
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.bookingService.fetchBooking().subscribe(() => {
      this.isLoading = false;
    });
  }


  ngOnDestroy() {
    if (this.bookingSub) {
      this.bookingSub.unsubscribe();
    }
  }

  onCancel(bookingID: string, slidinEl: IonItemSliding) {
    slidinEl.close();
    this.loadingCtrl.create({
      message: 'Cancelling booking session'
    }).then(loadingEl => {
      loadingEl.present();
      this.bookingService.cancelBooking(bookingID).subscribe(() => {
        loadingEl.dismiss();
      });
    });
  }
}
