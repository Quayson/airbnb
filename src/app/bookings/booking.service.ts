import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { take, map, tap, delay, switchMap } from 'rxjs/operators';

import { AuthService } from './../auth/auth.service';
import { Booking } from './booking.model';

interface BookingData {
  bookedFrom: string;
  bookedTo: string;
  firstName: string;
  guestNumber: number;
  lastName: string;
  placeID: string;
  placeImage: string;
  placeTitle: string;
  userID: string;
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private _bookings = new BehaviorSubject<Booking[]>([]);

  constructor(private authService: AuthService, private http: HttpClient) { }


  get bookings() {
    return this._bookings.asObservable();
  }

  addBooking(
    placeId: string,
    placeTitle: string,
    placeImage: string,
    firstName: string,
    lastName: string,
    guestNumber: number,
    dateFrom: Date,
    dateTo: Date
  ) {
    let generatedID: string;
    const newBooking = new Booking(
      Math.random().toString(),
      placeId,
      this.authService.userID,
      placeTitle,
      placeImage,
      firstName,
      lastName,
      guestNumber,
      dateFrom,
      dateTo
    );

    // Posting new booking to firebase server
    return this.http.post<{ name: string }>('https://pairbnb-9145c.firebaseio.com/bookings.json', { ...newBooking, id: null })
      .pipe(switchMap(resData => {
        generatedID = resData.name;
        return this.bookings;
      }), take(1), tap(bookings => {
        newBooking.id = generatedID;
        this._bookings.next(bookings.concat(newBooking));
      }));
  }


  // Cancelling a booking made by user
  cancelBooking(bookingID: string) {
    return this.http.delete(
      `https://pairbnb-9145c.firebaseio.com/bookings/${bookingID}.json`
    ).pipe(switchMap(() => {
      return this.bookings;
    }),
      take(1),
      tap(bookings => {
        this._bookings.next(bookings.filter(booking => booking.id !== bookingID));
      })
    );
  }


  // fetching bookings made by user
  fetchBooking() {
    return this.http
    .get<{ [key: string]: BookingData }>(
      `https://pairbnb-9145c.firebaseio.com/bookings.json?orderBy="userID"&equalTo="${
        this.authService.userID
      }"`
    )
    .pipe(
      map(bookingData => {
        const bookings = [];
        for (const key in bookingData) {
          if (bookingData.hasOwnProperty(key)) {
            bookings.push(
              new Booking(
                key,
                bookingData[key].placeID,
                bookingData[key].userID,
                bookingData[key].placeTitle,
                bookingData[key].placeImage,
                bookingData[key].firstName,
                bookingData[key].lastName,
                bookingData[key].guestNumber,
                new Date(bookingData[key].bookedFrom),
                new Date(bookingData[key].bookedTo)
              )
            );
          }
        }
        return bookings;
      }),
      tap(bookings => {
        this._bookings.next(bookings);
      })
    );
  }
}
